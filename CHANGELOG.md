Journal des modifications
=========================

6.0.5 (19/12/2023)
------------------

- [Fix] Un seul privilège peut être transmis sous forme de string à convertir en array 

6.0.1 (21/03/2023)
-----
- Mise en place du changelog
- Réintégration de l'AssertionFactory, à utiliser pour les assertions
- [Fix] erreur ns \InvalidArgumentException : \ ajouté

5.0.7 (05/05/2023)
-----
- Correction bug d'affichage sur le tableau d'affectation lorsque deux catégories possèdent le même libellé
