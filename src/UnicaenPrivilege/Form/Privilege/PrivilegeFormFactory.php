<?php 

namespace UnicaenPrivilege\Form\Privilege;

use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;

class PrivilegeFormFactory
{
    /**
     * Create form
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PrivilegeForm|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var PrivilegeCategorieService $categorieService
         * @var PrivilegeService $privilegeService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $categorieService = $container->get(PrivilegeCategorieService::class);
        $privilegeService = $container->get(PrivilegeService::class);
        $hydrator = $container->get('HydratorManager')->get(DoctrineObject::class);

        $form = new PrivilegeForm();
        $form->setEntityManager($entityManager);
        $form->setPrivilegeCategorieService($categorieService);
        $form->setPrivilegeService($privilegeService);
        $form->setHydrator($hydrator);

        return $form;
    }
}
