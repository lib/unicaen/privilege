<?php

namespace UnicaenPrivilege\Form\Privilege;

use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\Regex;

class CategorieForm extends Form
{
    use EntityManagerAwareTrait;
    use PrivilegeCategorieServiceAwareTrait;

    public function init()
    {
        $this->setAttribute('id', 'form-categorie');

        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'namespace',
            'options' => [
                'label' => "Namespace associé :",
            ],
            'attributes' => [
                'id' => 'namespace',
            ],
        ]);

        $this->add([
            'type' => Number::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre dans la liste :",
            ],
            'attributes' => [
                'id' => 'ordre',
            ],
        ]);

        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un libellé."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Ce libellé est déjà utilisé pour une autre catégorie.",
                            ],
                            'callback' => function ($value, $context = []) {
                                $categorie = $this->privilegeCategorieService->findByLibelle($value);
                                if(!$categorie) {
                                    return true;
                                }
                                elseif($this->getObject()->getId() != null) { // modification
                                    return strtoupper($this->getObject()->getLibelle()) === strtoupper($value);
                                }
                                return false;
                            },
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],

            'code' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un code."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Regex::class,
                        'options' => [
                            'pattern' => '/^[a-z0-9_-]+$/',
                            'messages' => [
                                Regex::NOT_MATCH => "Seuls les caractères suivants sont autorisés : [a-z, 0-9, _, -].",
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Ce code est déjà utilisé pour une autre catégorie.",
                            ],
                            'callback' => function ($value, $context = []) {
                                $categorie = $this->privilegeCategorieService->findByCode($value);
                                if(!$categorie) {
                                    return true;
                                }
                                elseif($this->getObject()->getId() != null) { // modification
                                    return strtoupper($this->getObject()->getCode()) === strtoupper($value);
                                }
                                return false;
                            },
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],

            'namespace' => [
                'required' => false,
            ],

            'ordre' => [
                'required' => false,
            ],
        ]));
    }
}