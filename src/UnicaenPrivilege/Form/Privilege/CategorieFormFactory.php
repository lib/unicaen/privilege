<?php

namespace UnicaenPrivilege\Form\Privilege;

use Doctrine\ORM\EntityManager;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;

class CategorieFormFactory
{
    /**
     * Create form
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CategorieForm|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var PrivilegeCategorieService $categorieService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $categorieService = $container->get(PrivilegeCategorieService::class);
        $hydrator = $container->get('HydratorManager')->get(DoctrineObject::class);

        $form = new CategorieForm();
        $form->setEntityManager($entityManager);
        $form->setPrivilegeCategorieService($categorieService);
        $form->setHydrator($hydrator);

        return $form;
    }
}
