<?php

namespace UnicaenPrivilege\Form\Privilege;

trait CategorieFiltreFormAwareTrait
{
    /**
     * @var CategorieFiltreForm $categorieFiltreForm
     */
    protected $categorieFiltreForm;

    /**
     * @param CategorieFiltreForm $categorieFiltreForm
     * @return CategorieFiltreForm
     */
    public function setCategorieFiltreForm(CategorieFiltreForm $categorieFiltreForm) : CategorieFiltreForm
    {
        $this->categorieFiltreForm = $categorieFiltreForm;

        return $this->categorieFiltreForm;
    }

    /**
     * @return CategorieFiltreForm
     */
    public function getCategorieFiltreForm() : CategorieFiltreForm
    {
        return $this->categorieFiltreForm;
    }
}