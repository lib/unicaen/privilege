<?php

namespace UnicaenPrivilege\Form\Privilege;

trait CategorieFormAwareTrait
{
    /**
     * @var CategorieForm
     */
    protected $categorieForm;


    /**
     * @param CategorieForm $categorieForm
     * @return CategorieForm
     */
    public function setCategorieForm(CategorieForm $categorieForm) : CategorieForm
    {
        $this->categorieForm = $categorieForm;

        return $this->categorieForm;
    }

    /**
     * @return CategorieForm
     */
    public function getCategorieForm() : CategorieForm
    {
        return $this->categorieForm;
    }
}