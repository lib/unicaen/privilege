<?php
//TODO prévoir le cas de l'édition ou le nom et le code peuvent rester identiques

namespace UnicaenPrivilege\Form\Privilege;

use DoctrineModule\Form\Element\ObjectSelect;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\Regex;

class PrivilegeForm extends Form
{
    use EntityManagerAwareTrait;
    use PrivilegeCategorieServiceAwareTrait;
    use PrivilegeServiceAwareTrait;

    public function init()
    {
        $this->setAttribute('id', 'form-privilege');

        $this->add([
            'type' => Hidden::class,
            'name' => 'categorieId',
        ]);

        $this->add([
            'type' => ObjectSelect::class,
            'name' => 'categorie',
            'options' => [
                'label' => "Catégorie :",
                'object_manager' => $this->getEntityManager(),
                'target_class' => $this->privilegeCategorieService->getEntityClass(),
                'property' => 'libelle',
                'find_method' => [
                    'name' => 'findBy',
                    'params' => [
                        'criteria' => [],
                        'orderBy' => ['libelle' => 'ASC'],
                    ],
                ],
                'disable_inarray_validator' => true,
            ],
            'attributes' => [
                'id' => 'categorie',
                'readonly' => true
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libelle :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'libelle',
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);

        $this->add([
            'type' => Number::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre dans la liste :",
            ],
            'attributes' => [
                'id' => 'ordre',
                'min' => 1
            ],
        ]);

        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un libellé."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Ce libellé est déjà utilisé pour un autre privilège de cette catégorie.",
                            ],
                            'callback' => function ($value, $context = []) {
                                $categorie = $this->privilegeCategorieService->find($context['categorieId']);
                                $privilege = $this->privilegeService->findByLibelle($value, $categorie);
                                if(!$privilege) {
                                    return true;
                                }
                                elseif($this->getObject()->getId() != null) { // modification
                                    return strtoupper($this->getObject()->getLibelle()) === strtoupper($value);
                                }
                                return false;
                            },
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],

            'code' => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un code."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Regex::class,
                        'options' => [
                            'pattern' => '/^[a-z0-9_-]+$/',
                            'messages' => [
                                Regex::NOT_MATCH => "Seuls les caractères suivants sont autorisés : [a-z, 0-9, _, -].",
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Callback::class,
                        'options' => [
                            'messages' => [
                                Callback::INVALID_VALUE => "Ce code est déjà utilisé pour un autre privilège de cette catégorie.",
                            ],
                            'callback' => function ($value, $context = []) {
                                $categorie = $this->privilegeCategorieService->find($context['categorieId']);
                                $privilege = $this->privilegeService->findByCode($value, $categorie);
                                if(!$privilege) {
                                    return true;
                                }
                                elseif($this->getObject()->getId() != null) { // modification
                                    return strtoupper($this->getObject()->getCode()) === strtoupper($value);
                                }
                                return false;
                            },
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],

            'categorie' => [
                'required' => false,
            ],

            'ordre' => [
                'required' => false,
            ],
        ]));
    }
}