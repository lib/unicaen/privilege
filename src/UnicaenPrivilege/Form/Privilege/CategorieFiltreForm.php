<?php

namespace UnicaenPrivilege\Form\Privilege;

use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class CategorieFiltreForm extends Form
{
    use EntityManagerAwareTrait;
    use PrivilegeCategorieServiceAwareTrait;

    public function init()
    {
        $this->setAttribute('method', 'get');

        $this->add([
            'type' => Select::class,
            'name' => 'namespace',
            'options' => [
                'empty_option' => "Tous les namespaces",
                'value_options' => $this->privilegeCategorieService->getListForSelect(
                    $this->privilegeCategorieService->findAllWithNamespace(['namespace' => 'ASC']), 'namespace', 'namespace'
                ),
            ],
            'attributes' => [
                'id' => 'namespace',
                'class' => "selectpicker show-tick",
                'data-live-search' => "true",
            ],
        ]);

        $this->add([
            'type' => Button::class,
            'name' => 'filtrer',
            'options' => [
                'label' => '<i class="fas fa-filter"></i> filtrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
                'id' => 'filtrer'
            ],
        ]);

        $this->add([
            'type' => Button::class,
            'name' => 'effacer',
            'options' => [
                'label' => '<i class="fas fa-backspace"></i> effacer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'id' => 'effacer'
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'namespace' => [
                'required' => false,
            ],
        ]));
    }
}