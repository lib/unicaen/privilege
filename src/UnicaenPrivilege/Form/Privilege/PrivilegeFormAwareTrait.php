<?php

namespace UnicaenPrivilege\Form\Privilege;

trait PrivilegeFormAwareTrait
{
    /**
     * @var PrivilegeForm $privilegeForm
     */
    protected $privilegeForm;

    /**
     * @param PrivilegeForm $privilegeForm
     * @return PrivilegeForm
     */
    public function setPrivilegeForm(PrivilegeForm $privilegeForm): PrivilegeForm
    {
        $this->privilegeForm = $privilegeForm;

        return $this->privilegeForm;
    }

    /**
     * @return PrivilegeForm
     */
    public function getPrivilegeForm(): PrivilegeForm
    {
        return $this->privilegeForm;
    }
}