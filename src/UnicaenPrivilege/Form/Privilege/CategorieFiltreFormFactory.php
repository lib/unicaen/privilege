<?php

namespace  UnicaenPrivilege\Form\Privilege;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;

class CategorieFiltreFormFactory
{
    /**
     * Create form
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CategorieFiltreForm|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var PrivilegeCategorieService $categorieService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $categorieService = $container->get(PrivilegeCategorieService::class);

        $form = new CategorieFiltreForm();
        $form->setEntityManager($entityManager);
        $form->setPrivilegeCategorieService($categorieService);

        return $form;
    }
}