<?php

namespace UnicaenPrivilege\Service;

use BjyAuthorize\Provider\Identity\ProviderInterface;
use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Service\UserContext;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Factory responsible of building the {@see \BjyAuthorize\Service\Authorize} service
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class AuthorizeServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new AuthorizeService($container->get('BjyAuthorize\Config'), $container);

//        /** @var ProviderInterface $identityProvider */
//        $identityProvider = $container->get('BjyAuthorize\Provider\Identity\ProviderInterface');

        /** @var UserContext $serviceUserContext */
        $serviceUserContext = $container->get(UserContext::class);

        $service->setServiceUserContext($serviceUserContext);
//        $service->setIdentityProvider($identityProvider);

        return $service;
    }
}
