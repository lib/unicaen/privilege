<?php

namespace UnicaenPrivilege\Service\Privilege;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Entity\Db\Privilege;
use UnicaenPrivilege\Service\Affectation\AffectationService;

class PrivilegeServiceFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PrivilegeService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var PrivilegeCategorieService $categorieService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $config = $container->get('Config');
        $categorieService = $container->get(PrivilegeCategorieService::class);

        $service = new PrivilegeService();
        $service->setEntityManager($entityManager);
        $service->setPrivilegeCategorieService($categorieService);
        $service->setEntityClass($config['unicaen-auth']['privilege_entity_class'] ?? Privilege::class);

        return $service;
    }
}