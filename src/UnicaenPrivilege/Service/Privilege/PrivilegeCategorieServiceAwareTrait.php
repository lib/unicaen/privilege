<?php

namespace UnicaenPrivilege\Service\Privilege;

trait PrivilegeCategorieServiceAwareTrait
{
    /**
     * @var PrivilegeCategorieService
     */
    private $privilegeCategorieService;

    /**
     * @param PrivilegeCategorieService $privilegeCategorieService
     * @return PrivilegeCategorieService
     */
    public function setPrivilegeCategorieService(PrivilegeCategorieService $privilegeCategorieService): PrivilegeCategorieService
    {
        $this->privilegeCategorieService = $privilegeCategorieService;

        return $this->privilegeCategorieService;
    }

    /**
     * @return PrivilegeCategorieService
     */
    public function getPrivilegeCategorieService(): PrivilegeCategorieService
    {
        return $this->privilegeCategorieService;
    }
}