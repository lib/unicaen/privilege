<?php

namespace UnicaenPrivilege\Service\Privilege;

trait PrivilegeServiceAwareTrait
{
    /**
     * @var PrivilegeService $privilegeService
     */
    protected $privilegeService;

    /**
     * @param PrivilegeService $privilegeService
     * @return PrivilegeService
     */
    public function setPrivilegeService(PrivilegeService $privilegeService): PrivilegeService
    {
        $this->privilegeService = $privilegeService;

        return $this->privilegeService;
    }

    /**
     * @return PrivilegeService
     */
    public function getPrivilegeService(): PrivilegeService
    {
        return $this->privilegeService;
    }
}