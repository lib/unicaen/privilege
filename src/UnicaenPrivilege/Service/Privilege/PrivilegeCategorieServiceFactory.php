<?php

namespace UnicaenPrivilege\Service\Privilege;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Entity\Db\Privilege;

class PrivilegeCategorieServiceFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PrivilegeCategorieService
     */
    public function  __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $config = $container->get('Config');

        $privilegeEntityClass = $config['unicaen-auth']['privilege_entity_class'] ?? Privilege::class;
        $mapping = $entityManager->getClassMetadata($privilegeEntityClass)->getAssociationMapping('categorie');

        $service = new PrivilegeCategorieService();
        $service->setEntityManager($entityManager);
        $service->setEntityClass($mapping['targetEntity']);

        return $service;
    }
}