<?php

namespace UnicaenPrivilege\Service\Privilege;

use BjyAuthorize\Provider\Resource\ProviderInterface as ResourceProviderInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use UnicaenApp\Controller\Plugin\Exception\InvalidArgumentException;
use UnicaenPrivilege\Entity\Db\PrivilegeCategorieInterface;
use UnicaenPrivilege\Entity\Db\PrivilegeInterface;
use UnicaenPrivilege\Exception\RuntimeException;
use UnicaenPrivilege\Provider\Privilege\PrivilegeProviderInterface;
use UnicaenPrivilege\Provider\Privilege\Privileges;
use UnicaenPrivilege\Service\CommonService;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use Laminas\Mvc\Controller\AbstractActionController;

class PrivilegeService extends CommonService implements PrivilegeProviderInterface, ResourceProviderInterface
{
    use PrivilegeCategorieServiceAwareTrait;

    /**
     * @var array
     */
    protected $privilegesRoles;


    /**
     * @param string $entityClass
     * @return void
     */
    public function setEntityClass(string $entityClass) : void
    {
        if (! class_exists($entityClass) || ! in_array(PrivilegeInterface::class, class_implements($entityClass))) {
            throw new InvalidArgumentException("L'entité associée aux privilèges doit implémenter " . PrivilegeInterface::class);
        }

        $this->entityClass = $entityClass;
    }

    /** ENTITY MANAGMENT **********************************************************************************************/

    /**
     * @param PrivilegeInterface $privilege
     * @return PrivilegeInterface
     */
    public function create(PrivilegeInterface $privilege) : PrivilegeInterface
    {
        try {
            $this->getEntityManager()->persist($privilege);
            $this->getEntityManager()->flush($privilege);
        } catch(ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la création du privilège.", null, $e);
        }

        return $privilege;
    }

    /**
     * @param PrivilegeInterface $privilege
     * @return PrivilegeInterface
     */
    public function update(PrivilegeInterface $privilege) : PrivilegeInterface
    {
        try {
            $this->getEntityManager()->flush($privilege);
        } catch(ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la mise à jour du privilège \"" . $privilege->getLibelle() . "\"", null, $e);
        }

        return $privilege;
    }

    /**
     * @param PrivilegeInterface $privilege
     * @return PrivilegeInterface
     */
    public function delete(PrivilegeInterface $privilege) : PrivilegeInterface
    {
        try {
            $this->getEntityManager()->remove($privilege);
            $this->getEntityManager()->flush($privilege);
        } catch(ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression du privilège \"" . $privilege->getLibelle() . "\"", null, $e);
        }

        return $privilege;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @param string $code
     * @param int|PrivilegeCategorieInterface $categorie
     * @return PrivilegeInterface|null
     */
    public function findByCode(string $code, $categorie) : ?PrivilegeInterface
    {
        return $this->getRepo()->findOneBy(['code' => $code, 'categorie' => $categorie]);
    }

    /**
     * @param string $code
     * @return PrivilegeInterface|null
     */
    public function findByFullCode(string $code) : ?PrivilegeInterface
    {
        [$categorieCode, $privilegeCode] = explode('-', $code);
        $categorie = $this->getPrivilegeCategorieService()->findByCode($categorieCode);
        if ($categorie === null) return null;
        $privilege = $this->findByCode($privilegeCode, $categorie);
        return $privilege;
    }

    /**
     * @param string $libelle
     * @param int|PrivilegeCategorieInterface $categorie
     * @return PrivilegeInterface|null
     */
    public function findByLibelle(string $libelle, $categorie) : ?PrivilegeInterface
    {
        return $this->getRepo()->findOneBy(['libelle' => $libelle, 'categorie' => $categorie]);
    }

    /**
     * @param PrivilegeCategorieInterface $categorie
     * @param array $orderBy
     * @return array
     */
    public function findByCategorie(PrivilegeCategorieInterface $categorie, array $orderBy = ['ordre' => 'ASC']) : array
    {
        return $this->getRepo()->findBy(['categorie' => $categorie], $orderBy);
    }

    /**
     * @param string $namespace
     * @return array
     */
    public function listByCategorie(?string $namespace)
    {
        $categories = $namespace
            ? $this->privilegeCategorieService->findByNamespace($namespace)
            : $this->privilegeCategorieService->findAll();

        $privilegesByCategorie = [];
        foreach ($categories as $c) {
            $privileges = $this->findByCategorie($c);
            if(count($privileges) > 0) {
                $privilegesByCategorie[$c->getId()] = $privileges;
            }
        }

        return $privilegesByCategorie;
    }



    /**
     * @param RoleInterface $role
     * @param PrivilegeInterface $privilege
     * @return void
     */
    public function toggle(RoleInterface $role, PrivilegeInterface $privilege)
    {
        if ($privilege->hasRole($role)) {
            $privilege->removeRole($role);
        } else {
            $privilege->addRole($role);
        }

        try {
//            $this->getEntityManager()->flush();
            $this->update($privilege);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors du changement de privilège \"" . $privilege->getLibelle() . "\" pour le rôle \"" . $role->getLibelle() . "\".", $e);
        }
    }

    /**
     * @param AbstractActionController $controller
     * @param string $paramName
     * @return PrivilegeInterface|null
     */
    public function getRequestedPrivilege(AbstractActionController $controller, string $paramName='privilege')
    {
        $id = $controller->params()->fromRoute($paramName);
        $result = $this->find($id);

        return $result;
    }

    /**
     * @return array
     */
    public function getPrivilegesRoles() : array
    {
        if (null === $this->privilegesRoles) {
            $this->privilegesRoles = [];

            foreach ($this->getRepo()->findAll() as $privilege) {
                /* @var $privilege PrivilegeInterface */
                $pr = [];
                foreach ($privilege->getRoles() as $role) {
                    /* @var $role RoleInterface */
                    $pr[] = $role->getRoleId();
                }
                $this->privilegesRoles[$privilege->getFullCode()] = $pr;
            }
        }

        return $this->privilegesRoles;
    }

    /**
     * @return array
     */
    public function getResources() : array
    {
        $resources = [];
        $privileges = array_keys($this->getPrivilegesRoles());
        foreach ($privileges as $privilege) {
            $resources[] = Privileges::getResourceId($privilege);
        }

        return $resources;
    }

    /**
     * @param PrivilegeCategorieInterface $categorie
     * @return float|int|mixed|string
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getMaxOrdreByCategorie(PrivilegeCategorieInterface $categorie)
    {
        $qb = $this->getRepo()->createQueryBuilder('p');
        $qb ->select($qb->expr()->max('p.ordre'))
            ->andWhere($qb->expr()->eq('p.categorie', ':categorie'))
                ->setParameter('categorie', $categorie);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /** FACADE ********************************************************************************************************/

    public function checkPrivilege(string $privilege, RoleInterface $role) : bool
    {
        [$catCode, $priCode] = explode('-', $privilege);
        $categorie = $this->getPrivilegeCategorieService()->findByCode($catCode);
        $pprivilege = $this->findByCode($priCode, $categorie->getId());
        if ($pprivilege === null) return false;
        $listings = $pprivilege->getRoles()->toArray();
        if (!in_array($role, $listings)) return false;
        return true;
    }
}

