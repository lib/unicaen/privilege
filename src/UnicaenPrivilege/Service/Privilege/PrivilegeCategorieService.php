<?php

namespace UnicaenPrivilege\Service\Privilege;

use Doctrine\ORM\ORMException;
use UnicaenApp\Exception\RuntimeException;
use UnicaenPrivilege\Entity\Db\PrivilegeCategorieInterface;
use UnicaenPrivilege\Service\CommonService;
use Laminas\Mvc\Controller\AbstractActionController;

class PrivilegeCategorieService extends CommonService
{
    /**
     * @param string $categorieEntityClass
     */
    public function setEntityClass($categorieEntityClass)
    {
        if (! class_exists($categorieEntityClass) || ! in_array(PrivilegeCategorieInterface::class, class_implements($categorieEntityClass))) {
            throw new \InvalidArgumentException("L'entité associée aux catégories de privilège doit implémenter " . PrivilegeCategorieInterface::class);
        }

        $this->entityClass = $categorieEntityClass;
    }

    /**
     * @param array $orderBy
     * @return PrivilegeCategorieInterface[]
     */
    public function findAll(array $orderBy = ['ordre' => 'ASC'])
    {
        return $this->getRepo()->findBy([], $orderBy);
    }

    /**
     * @param array $orderBy
     * @return float|int|mixed|string
     */
    public function findAllWithNamespace(array $orderBy = ['ordre' => 'ASC'])
    {
        $qb = $this->getRepo()->createQueryBuilder('c');
        $qb ->distinct()
            ->andWhere($qb->expr()->isNotNull('c.namespace'));
        foreach ($orderBy as $sort => $order){
            $qb = $qb->addOrderBy('c.'.$sort, $order);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $code
     * @return CategorieInterface|null
     */
    public function findByCode(string $code)
    {
        return $this->getRepo()->findOneBy(['code' => $code]);
    }

    /**
     * @param string $libelle
     * @return CategorieInterface|null
     */
    public function findByLibelle(string $libelle)
    {
        return $this->getRepo()->findOneBy(['libelle' => $libelle]);
    }

    /**
     * @param string $namespace
     * @param array $orderBy
     * @return array
     */
    public function findByNamespace(string $namespace, array $orderBy = ['ordre' => 'ASC'])
    {
        return $this->getRepo()->findBy(['namespace' => $namespace]);
    }

    /**
     * @param PrivilegeCategorieInterface $categorie
     * @return PrivilegeCategorieInterface
     */
    public function create(PrivilegeCategorieInterface $categorie)
    {
        try {
            $this->getEntityManager()->persist($categorie);
            $this->getEntityManager()->flush($categorie);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la création de la catégorie de privilège.", null, $e);
        }

        return $categorie;
    }

    /**
     * @param PrivilegeCategorieInterface $categorie
     * @return PrivilegeCategorieInterface
     */
    public function update(PrivilegeCategorieInterface $categorie)
    {
        try {
            $this->getEntityManager()->flush($categorie);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la mise à jour de la catégorie de privilège \"" . $categorie->getLibelle() . "\"", null, $e);
        }

        return $categorie;
    }

    /**
     * @param PrivilegeCategorieInterface $categorie
     * @return PrivilegeCategorieInterface
     */
    public function delete(PrivilegeCategorieInterface $categorie) : PrivilegeCategorieInterface
    {
        try {
            $this->getEntityManager()->remove($categorie);
            $this->getEntityManager()->flush($categorie);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression de la catégorie de privilège \"" . $categorie->getLibelle() . "\"", null, $e);
        }

        return $categorie;
    }


    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return PrivilegeCategorieInterface|null
     */
    public function getRequestedCategorie(AbstractActionController $controller, string $param = 'categorie')
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->find($id);

        return $result;
    }
}