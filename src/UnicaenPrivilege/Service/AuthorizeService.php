<?php

namespace UnicaenPrivilege\Service;

use UnicaenAuthentification\Service\Traits\UserContextServiceAwareTrait;

/**
 * Authorize service
 *
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class AuthorizeService extends \BjyAuthorize\Service\Authorize
{
    use UserContextServiceAwareTrait;


    /**
     *
     * @return string
     */
    public function getIdentity()
    {
        $this->loaded && $this->loaded->__invoke();
        if ($this->loading) return 'bjyauthorize-identity';

        // on retourne par défaut le rôle sélectionné
        $role = $this->serviceUserContext->getSelectedIdentityRole();
        if ($role) return $role;

        $roles = $this->getIdentityProvider()->getIdentityRoles();
        // sinon, si on est uniquement authentifié et que user est défini, on retourne le rôle user
        if (isset($roles['user'])) return $roles['user'];
        // sinon, si guest est défini alors on retourne guest
        if (isset($roles['guest'])) return $roles['guest'];

        // sinon rien du tout!!
        return null;
    }

}
