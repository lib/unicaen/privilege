<?php

namespace UnicaenPrivilege\Service;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use UnicaenApp\Service\EntityManagerAwareTrait;

abstract class CommonService
{
    use EntityManagerAwareTrait;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * Injecte la classe de l'entité liée au service
     *
     * @param string $entityClass
     * @return void
     */
    abstract public function setEntityClass(string $entityClass);

    /**
     * Retourne le nom de la classe de l'entité liée au service
     *
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Retourne une nouvelle instance de l'entité liée au service
     *
     * @return mixed
     */
    public function getEntityInstance()
    {
        return new $this->entityClass;
    }

    /**
     * Retourne le dépôt de l'entité liée au service
     *
     * @return EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository($this->entityClass);
    }

    /**
     * @param mixed $id
     * @return object|null
     */
    public function find($id)
    {
        return $this->getRepo()->find($id);
    }

    /**
     * Retourne une liste d'entités sous forme d'un tableau associatif dont les clés
     * sont les id des entités et les valeurs correspondent au champ choisi
     *
     * @param array $entities liste des entités
     * @param string $key attribut utilisé comme clé
     * @param string $value attribut utilisé pour les valeurs
     * @param string $entityClass classe de l'entité
     * @return array
     */
    public function getListForSelect($entities, $key = 'id', $value = 'libelle', $entityClass = null)
    {
        $entityClass = $entityClass ?: $this->getEntityClass();

        foreach ($entities as $entity) {
            if ($entity instanceof $entityClass
                && method_exists($entity, $kgetter = 'get' . ucfirst($key))
                && method_exists($entity, $vgetter = 'get' . ucfirst($value))) {
                $result[$entity->$kgetter()] = $entity->$vgetter();
            }
        }

        return (array)$result;
    }

    /**
     * @return float|int|mixed|string
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getMaxOrdre()
    {
        $qb = $this->getRepo()->createQueryBuilder('e');
        $qb->select($qb->expr()->max('e.ordre'));

        return $qb->getQuery()->getSingleScalarResult();
    }
}