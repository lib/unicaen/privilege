<?php

namespace UnicaenPrivilege\Entity\Db\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use UnicaenApp\Service\EntityManagerAwareTrait;

class PrivilegeRepository extends EntityRepository
{
    use EntityManagerAwareTrait;

    public function createQueryBuilder($alias, $indexBy = null): QueryBuilder
    {
        $qb = parent::createQueryBuilder($alias, $indexBy);
        $qb
            ->leftjoin($alias . '.roles', 'role')->addSelect('role')
            ->leftJoin($alias . '.categorie', 'categorie')->addSelect('categorie');
        return $qb;
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $qb = $this->createQueryBuilder('privilege')
            ->andWhere('privilege.id = :id')->setParameter('id', $id)
        ;
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function findAll()
    {
        $qb = $this->createQueryBuilder('privilege')
        ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }
}