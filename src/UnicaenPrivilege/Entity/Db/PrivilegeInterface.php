<?php

namespace UnicaenPrivilege\Entity\Db;

use Doctrine\Common\Collections\Collection;
use UnicaenPrivilege\Provider\Privilege\Privileges;
use UnicaenUtilisateur\Entity\Db\RoleInterface;

interface PrivilegeInterface
{
    /**
     * @return string
     */
    public function __toString();

    /**
     * Get id.
     *
     * @return int
     */
    public function getId();

    /**
     * Set id.
     *
     * @param int $id
     * @return PrivilegeInterface
     */
    public function setId($id);

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode();

    /**
     * Set code.
     *
     * @param string $code
     * @return PrivilegeInterface
     */
    public function setCode($code);

    /**
     * Get code with category.
     *
     * @return string
     */
    public function getFullCode();

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle();

    /**
     * Set libelle.
     *
     * @param string $libelle
     * @return PrivilegeInterface
     */
    public function setLibelle($libelle);

    /**
     * Get ordre.
     *
     * @return integer
     */
    function getOrdre();

    /**
     * Set ordre.
     *
     * @param integer $ordre
     * @return PrivilegeInterface
     */
    function setOrdre($ordre);


    /**
     * Get categorie.
     *
     * @return PrivilegeCategorieInterface
     */
    public function getCategorie();

    /**
     * Set categorie.
     *
     * @param PrivilegeCategorieInterface $categorie
     * @return PrivilegeInterface
     */
    public function setCategorie(PrivilegeCategorieInterface $categorie);

    /**
     * @return Collection
     */
    public function getRoles();

    /**
     * Add role.
     *
     * @param RoleInterface $role
     * @return PrivilegeInterface
     */
    public function addRole(RoleInterface $role);

    /**
     * Remove role.
     *
     * @param RoleInterface $role
     * @return PrivilegeInterface
     */
    public function removeRole(RoleInterface $role);

    /**
     * Check privilege role
     *
     * @param RoleInterface $role
     * @return bool
     */
    function hasRole(RoleInterface $role);

    /**
     * @return string
     */
    public function getResourceId();
}