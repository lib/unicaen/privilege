<?php

namespace UnicaenPrivilege\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

abstract class AbstractPrivilegeCategorie implements PrivilegeCategorieInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $libelle;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var int
     */
    protected $ordre;

    /**
     * @var ArrayCollection
     */
    protected $privileges;


    /**
     * Categorie constructor
     */
    public function __construct()
    {
        $this->privileges = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLibelle();
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     * @return PrivilegeCategorieInterface
     */
    public function setId($id)
    {
        $this->id = (int)$id;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code.
     *
     * @param string $code
     * @return PrivilegeCategorieInterface
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     * @return PrivilegeCategorieInterface
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get namespace.
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set namespace.
     *
     * @param string $namespace
     * @return PrivilegeCategorieInterface
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     * @return PrivilegeCategorieInterface
     */
    public function setOrdre($ordre)
    {
        $this->ordre = (int)$ordre;

        return $this;
    }

    /**
     * Get privileges.
     *
     * @return Collection
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }

    /**
     * Add privilege.
     *
     * @param PrivilegeInterface $privilege
     * @return PrivilegeCategorieInterface
     */
    public function addPrivilege(PrivilegeInterface $privilege)
    {
        $this->privileges->add($privilege);

        return $this;
    }

    /**
     * Remove privilege.
     *
     * @param PrivilegeInterface $privilege
     * @return PrivilegeCategorieInterface
     */
    public function removePrivilege(PrivilegeInterface $privilege)
    {
        $this->privileges->removeElement($privilege);

        return $this;
    }

    /**
     * Get privileges class name
     *
     * @return string
     */
    public function getClassname()
    {
        return ucfirst(strtolower($this->getCode())) . "Privileges";
    }
}