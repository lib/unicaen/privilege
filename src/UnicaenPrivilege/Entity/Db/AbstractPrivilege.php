<?php

namespace UnicaenPrivilege\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenPrivilege\Provider\Privilege\Privileges;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use Laminas\Permissions\Acl\Resource\ResourceInterface;

abstract class AbstractPrivilege implements PrivilegeInterface, ResourceInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $libelle;

    /**
     * @var int
     */
    protected $ordre;

    /**
     * @var PrivilegeCategorieInterface
     */
    protected $categorie;

    /**
     * @var Collection
     */
    protected $roles;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLibelle();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     * @return PrivilegeInterface
     */
    public function setId($id)
    {
        $this->id = (int)$id;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code.
     *
     * @param string $code
     * @return PrivilegeInterface
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code with category.
     *
     * @return string
     */
    public function getFullCode()
    {
        return sprintf('%s-%s', $this->categorie->getCode(), $this->getCode());
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     * @return PrivilegeInterface
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return integer
     */
    function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set ordre.
     *
     * @param integer $ordre
     * @return PrivilegeInterface
     */
    function setOrdre($ordre)
    {
        $this->ordre = (int)$ordre;

        return $this;
    }


    /**
     * Get categorie.
     *
     * @return PrivilegeCategorieInterface
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set categorie.
     *
     * @param PrivilegeCategorieInterface $categorie
     * @return PrivilegeInterface
     */
    public function setCategorie(PrivilegeCategorieInterface $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add role.
     *
     * @param RoleInterface $role
     * @return PrivilegeInterface
     */
    public function addRole(RoleInterface $role)
    {
        $this->roles->add($role);
        $role->addPrivilege($this);

        return $this;
    }

    /**
     * Remove role.
     *
     * @param RoleInterface $role
     * @return PrivilegeInterface
     */
    public function removeRole(RoleInterface $role)
    {
        $this->roles->removeElement($role);
        $role->removePrivilege($this);
        return $this;
    }

    /**
     * Check privilege role
     *
     * @param RoleInterface $role
     * @return bool
     */
    function hasRole(RoleInterface $role)
    {
        foreach ($this->roles as $r) {
            if ($r === $role) return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getResourceId()
    {
        return Privileges::getResourceId($this);
    }
}