<?php

namespace UnicaenPrivilege\Entity\Db;

use Doctrine\Common\Collections\Collection;

interface PrivilegeCategorieInterface
{
    /**
     * @return string
     */
    public function __toString();

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId();

    /**
     * Set id.
     *
     * @param int $id
     * @return PrivilegeCategorieInterface
     */
    public function setId($id);

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode();

    /**
     * Set code.
     *
     * @param string $code
     * @return PrivilegeCategorieInterface
     */
    public function setCode($code);

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle();

    /**
     * Set libelle.
     *
     * @param string $libelle
     * @return PrivilegeCategorieInterface
     */
    public function setLibelle($libelle);

    /**
     * Get namespace.
     *
     * @return string
     */
    public function getNamespace();

    /**
     * Set namespace.
     *
     * @param string $namespace
     * @return PrivilegeCategorieInterface
     */
    public function setNamespace($namespace);

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre();

    /**
     * Set ordre.
     *
     * @param int $ordre
     * @return PrivilegeCategorieInterface
     */
    public function setOrdre($ordre);

    /**
     * Get privileges.
     *
     * @return Collection
     */
    public function getPrivileges();

    /**
     * Add privilege.
     *
     * @param PrivilegeInterface $privilege
     * @return PrivilegeCategorieInterface
     */
    public function addPrivilege(PrivilegeInterface $privilege);

    /**
     * Remove privilege.
     *
     * @param PrivilegeInterface $privilege
     * @return PrivilegeCategorieInterface
     */
    public function removePrivilege(PrivilegeInterface $privilege);

    /**
     * Get privileges class name
     *
     * @return string
     */
    public function getClassname();
}