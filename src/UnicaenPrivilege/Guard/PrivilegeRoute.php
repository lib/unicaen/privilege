<?php

namespace UnicaenPrivilege\Guard;

use BjyAuthorize\Guard\Route;
use Laminas\ServiceManager\ServiceLocatorInterface;
use UnicaenPrivilege\Provider\Privilege\PrivilegeProviderAwareTrait;
use UnicaenApp\Traits\SessionContainerTrait;


/**
 * Description of PrivilegeRoute
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class PrivilegeRoute extends Route
{
    use PrivilegeProviderAwareTrait;
    use SessionContainerTrait;

    public function __construct(array $rules, ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        parent::__construct($rules, $serviceLocator);
    }

    public function processConfig()
    {
        $this->rules = $this->privilegesToRoles($this->config);
    }

    protected function privilegesToRoles(array $rules) : array
    {
        $pr = $this->getPrivilegeProvider()->getPrivilegesRoles();

        foreach ($rules as $index => $rule) {
            if (isset($rule['privileges'])) {
                $rolesCount    = 0;
                $privileges    = (array)$rule['privileges'];
                $rule['roles'] = isset($rule['roles']) ? (array)$rule['roles'] : [];
                foreach ($pr as $privilege => $roles) {
                    if (in_array($privilege, $privileges)) {
                        $rolesCount += count($roles);
                        $rule['roles'] = array_unique(array_merge($rule['roles'], $roles));
                    }
                }
                unset($rule['privileges']);
                if (0 < count($rule['roles'])) {
                    $rules[$index] = $rule;
                }else{
                    unset($rules[$index]);
                }
            }
        }

        return $rules;
    }

    public static function getResourceId($route) : string
    {
        return 'route/'.$route;
    }

}