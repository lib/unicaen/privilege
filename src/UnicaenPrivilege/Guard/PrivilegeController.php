<?php

namespace UnicaenPrivilege\Guard;

use BjyAuthorize\Guard\Controller;
use UnicaenPrivilege\Provider\Privilege\PrivilegeProviderAwareTrait;
use UnicaenApp\Traits\SessionContainerTrait;

/**
 * Description of PrivilegeController
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class PrivilegeController extends Controller
{
    use PrivilegeProviderAwareTrait;
    use SessionContainerTrait;

    /**
     * {@inheritdoc}
     */
    public function processConfig()
    {
        $this->config = $this->privilegesToRoles($this->config);

        parent::processConfig();
    }

    /**
     * @param array $rules
     * @return array
     */
    protected function privilegesToRoles(array $rules) : array
    {
        $pr = $this->getPrivilegeProvider()->getPrivilegesRoles();

        foreach ($rules as $index => $rule) {
            if (isset($rule['privileges'])) {
                $rolesCount    = 0;
                $privileges    = (array)$rule['privileges'];
                $rule['roles'] = isset($rule['roles']) ? (array)$rule['roles'] : [];
                foreach ($pr as $privilege => $roles) {
                    if (in_array($privilege, $privileges)) {
                        $rolesCount += count($roles);
                        $rule['roles'] = array_unique(array_merge($rule['roles'], $roles));
                    }
                }
                unset($rule['privileges']);
                if (0 < count($rule['roles'])) {
                    $rules[$index] = $rule;
                }else{
                    unset($rules[$index]);
                }
            }
        }

        return $rules;
    }

    public static function getResourceId($controller, $action = null) : string
    {
        if (isset($action)) {
            return sprintf('controller/%s:%s', $controller, strtolower($action));
        }

        return sprintf('controller/%s', $controller);
    }

}