<?php

namespace UnicaenPrivilege\Provider\Privilege;

use UnicaenPrivilege\Entity\Db\PrivilegeInterface;

class Privileges {

    /**
     * Retourne le nom de la ressource associée au privilège donné
     *
     * @param PrivilegeInterface|string $privilege
     * @return string
     */
    public static function getResourceId($privilege)
    {
        if ($privilege instanceof PrivilegeInterface) {
            $privilege = $privilege->getFullCode();
        }

        return 'privilege/' . $privilege;
    }

}