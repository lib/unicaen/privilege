<?php

namespace UnicaenPrivilege\Provider\Privilege;

class PrivilegePrivileges extends Privileges
{
    const PRIVILEGE_VOIR = 'privilege-privilege_voir';
    const PRIVILEGE_AJOUTER = 'privilege-privilege_ajouter';
    const PRIVILEGE_MODIFIER = 'privilege-privilege_modifier';
    const PRIVILEGE_SUPPRIMER = 'privilege-privilege_supprimer';
    const PRIVILEGE_AFFECTER = 'privilege-privilege_affecter';
}