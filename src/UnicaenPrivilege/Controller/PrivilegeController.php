<?php

namespace UnicaenPrivilege\Controller;

use UnicaenPrivilege\Form\Privilege\CategorieFiltreFormAwareTrait;
use UnicaenPrivilege\Form\Privilege\PrivilegeFormAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use UnicaenUtilisateur\Entity\Db\AbstractRole;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class PrivilegeController extends AbstractActionController
{
    use PrivilegeCategorieServiceAwareTrait;
    use PrivilegeServiceAwareTrait;
    use RoleServiceAwareTrait;
    use PrivilegeFormAwareTrait;
    use CategorieFiltreFormAwareTrait;

    public function indexAction() : array
    {
        $form = $this->categorieFiltreForm;
        $namespace = $this->params()->fromQuery('namespace');
        if($namespace) {
            $form->get('namespace')->setValue($namespace);
        }

        $roles = $this->roleService->findAll();

        //filtrage des rôles non affichés
        $roles = array_filter($roles, function (AbstractRole $role) { return $role->isDisplayed(); });

        $privilegesByCategorie = $this->privilegeService->listByCategorie($namespace);

        $title = "Attribution des privilèges";
        return compact('title', 'form', 'roles', 'privilegesByCategorie', 'namespace');
    }

    public function ajouterAction() : ViewModel
    {
        $categorie = $this->privilegeCategorieService->getRequestedCategorie($this);
        $privilege = $this->privilegeService->getEntityInstance();
        $privilege->setOrdre($this->privilegeService->getMaxOrdreByCategorie($categorie)+1);
        $privilege->setCategorie($categorie);
        $form = $this->privilegeForm;
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-privilege/ajouter', ['categorie' => $categorie->getId()], [], true));
        $form->get('categorieId')->setValue($categorie->getId());
        $form->bind($privilege);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->privilegeService->create($privilege);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Privilège <strong>%s</strong> créé avec succès.", $privilege->getLibelle()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la création du privilège <strong>%s</strong>.", $privilege->getLibelle()));
                }
            }
        }

        $view = new ViewModel();
        $view->setTemplate('unicaen-privilege/privilege/template/form-privilege');
        $view->setVariables([
            'title' => "Ajout d'un nouveau privilège",
            'form' => $form,
        ]);

        return $view;
    }

    public function modifierAction() : ViewModel
    {
        $privilege = $this->privilegeService->getRequestedPrivilege($this);
        $form = $this->privilegeForm;
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-privilege/modifier', ['privilege' => $privilege->getId()], [], true));
        $form->get('categorieId')->setValue($privilege->getCategorie()->getId());
        $form->bind($privilege);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->privilegeService->update($privilege);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Privilège <strong>%s</strong> modifié avec succès.", $privilege->getLibelle()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la modification du privilège <strong>%s</strong>.", $privilege->getLibelle()));
                }
            }
        }

        $view = new ViewModel();
        $view->setTemplate('unicaen-privilege/privilege/template/form-privilege');
        $view->setVariables([
            'title' => "Modification privilège",
            'form' => $form,
        ]);

        return $view;
    }

    public function attribuerAction(): ViewModel
    {
        $privilege = $this->privilegeService->getRequestedPrivilege($this);
        $role = $this->roleService->getRequestedRole($this);
        $this->privilegeService->toggle($role,$privilege);

        $viewModel = new ViewModel();
        $viewModel
            ->setVariables(compact('privilege', 'role'))
            ->setTerminal(true);

        return $viewModel;
    }

    public function supprimerAction() : array
    {
        try {
            $privilege = $this->privilegeService->getRequestedPrivilege($this);
            $this->privilegeService->delete($privilege);
            $this->flashMessenger()->addSuccessMessage(sprintf("Privilège <strong>%s</strong> supprimé avec succès.", $privilege->getLibelle()));
        } catch (Exception $e) {
            $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la suppression du privilège <strong>%s</strong>.", $privilege->getLibelle()));
        }

        return compact('privilege');
    }
}