<?php

namespace UnicaenPrivilege\Controller;

use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Form\Privilege\CategorieFiltreForm;
use UnicaenPrivilege\Form\Privilege\PrivilegeForm;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;
use UnicaenUtilisateur\Service\Role\RoleService;

class PrivilegeControllerFactory
{
    /**
     * Create controller
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PrivilegeController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var PrivilegeCategorieService $categorieService
         * @var PrivilegeService $privilegeService
         * @var RoleService $roleService
         */
        $categorieService = $container->get(PrivilegeCategorieService::class);
        $privilegeService = $container->get(PrivilegeService::class);
        $roleService = $container->get(RoleService::class);
        $privilegeForm = $container->get('FormElementManager')->get(PrivilegeForm::class);
        $categorieFiltreForm = $container->get('FormElementManager')->get(CategorieFiltreForm::class);

        $controller = new PrivilegeController();
        $controller->setPrivilegeCategorieService($categorieService);
        $controller->setPrivilegeService($privilegeService);
        $controller->setRoleService($roleService);
        $controller->setPrivilegeForm($privilegeForm);
        $controller->setCategorieFiltreForm($categorieFiltreForm);

        return $controller;
    }
}