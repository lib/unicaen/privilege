<?php

namespace UnicaenPrivilege\Controller;

use UnicaenPrivilege\Form\Privilege\CategorieFiltreFormAwareTrait;
use UnicaenPrivilege\Form\Privilege\CategorieFormAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class PrivilegeCategorieController extends AbstractActionController
{
    use PrivilegeCategorieServiceAwareTrait;
    use PrivilegeServiceAwareTrait;
    use CategorieFormAwareTrait;
    use CategorieFiltreFormAwareTrait;

    public function indexAction() : array
    {
        $form = $this->categorieFiltreForm;
        $namespace = $this->params()->fromQuery('namespace');
        if($namespace) {
            $form->get('namespace')->setValue($namespace);
            $categories = $this->privilegeCategorieService->findByNamespace($namespace);
        }
        else {
            $categories = $this->privilegeCategorieService->findAll();
        }

        $title = "Gestion des catégories de privilèges";
        return compact('title', 'form', 'categories');
    }

    public function ajouterAction() : ViewModel
    {
        $categorie = $this->privilegeCategorieService->getEntityInstance();
        $categorie->setOrdre($this->privilegeCategorieService->getMaxOrdre()+1);
        $form = $this->categorieForm;
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-privilege/categorie/ajouter', [], [], true));
        $form->bind($categorie);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->privilegeCategorieService->create($categorie);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Catégorie <strong>%s</strong> créée avec succès.", $categorie->getLibelle()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la création de la catégorie <strong>%s</strong>.", $categorie->getLibelle()));
                }
            }
        }

        $view = new ViewModel();
        $view->setTemplate('unicaen-privilege/privilege-categorie/template/form-categorie');
        $view->setVariables([
            'title' => "Ajout d'une nouvelle catégorie",
            'form' => $form,
        ]);

        return $view;
    }

    public function gererAction() : array
    {
        $categorie = $this->privilegeCategorieService->getRequestedCategorie($this);
        $privileges = $this->privilegeService->findByCategorie($categorie);
        $title = "Gestion de la catégorie";

        return compact('title', 'categorie', 'privileges');
    }

    public function providerAction() : array
    {
        $categorie = $this->privilegeCategorieService->getRequestedCategorie($this);

        return compact('categorie');
    }

    public function modifierAction() : ViewModel
    {
        $categorie = $this->privilegeCategorieService->getRequestedCategorie($this);
        $form = $this->categorieForm;
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-privilege/categorie/modifier', ['categorie' => $categorie->getId()], [], true));
        $form->bind($categorie);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->privilegeCategorieService->update($categorie);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Catégorie <strong>%s</strong> modifiée avec succès.", $categorie->getLibelle()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la modification de la catégorie <strong>%s</strong>.", $categorie->getLibelle()));
                }
            }
        }

        $view = new ViewModel();
        $view->setTemplate('unicaen-privilege/privilege-categorie/template/form-categorie');
        $view->setVariables([
            'title' => "Modification de la catégorie",
            'form' => $form,
        ]);

        return $view;
    }


    public function supprimerAction() : array
    {
        try {
            $categorie = $this->privilegeCategorieService->getRequestedCategorie($this);
            $this->privilegeCategorieService->delete($categorie);
            $this->flashMessenger()->addSuccessMessage(sprintf("Catégorie <strong>%s</strong> supprimée avec succès.", $categorie->getLibelle()));
        } catch (Exception $e) {
            $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la suppression de la catégorie <strong>%s</strong>.", $categorie->getLibelle()));
        }

        return compact('categorie');
    }
}