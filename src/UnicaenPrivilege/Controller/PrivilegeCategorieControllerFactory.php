<?php

namespace UnicaenPrivilege\Controller;

use Interop\Container\ContainerInterface;
use UnicaenPrivilege\Form\Privilege\CategorieFiltreForm;
use UnicaenPrivilege\Form\Privilege\CategorieForm;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;

class PrivilegeCategorieControllerFactory
{
    /**
     * Create controller
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PrivilegeCategorieController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var PrivilegeCategorieService $categorieService
         * @var PrivilegeService $privilegeService
         */
        $categorieService = $container->get(PrivilegeCategorieService::class);
        $privilegeService = $container->get(PrivilegeService::class);
        $categorieForm = $container->get('FormElementManager')->get(CategorieForm::class);
        $categorieFiltreForm = $container->get('FormElementManager')->get(CategorieFiltreForm::class);

        $controller = new PrivilegeCategorieController();
        $controller->setPrivilegeCategorieService($categorieService);
        $controller->setPrivilegeService($privilegeService);
        $controller->setCategorieForm($categorieForm);
        $controller->setCategorieFiltreForm($categorieFiltreForm);

        return $controller;
    }
}