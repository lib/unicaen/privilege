<?php

namespace UnicaenPrivilege\View\Privilege;

use Application\Entity\Db\Profil;
use Application\Entity\Db\Service;
use Application\Provider\Privilege\ProfilPrivileges;
use UnicaenPrivilege\Entity\Db\PrivilegeInterface;
use UnicaenPrivilege\Provider\Privilege\PrivilegePrivileges;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use Laminas\View\Helper\AbstractHelper;

class PrivilegeViewHelper extends AbstractHelper
{
    /**
     * @var PrivilegeInterface
     */
    private $privilege;


    /**
     * @param PrivilegeInterface $privilege
     * @return self
     */
    public function __invoke($privilege = null)
    {
        $this->privilege = $privilege;

        return $this;
    }

    /**
     * @param RoleInterface $role
     * @return string
     */
    public function renderLienAjouter($role)
    {
        return $this->view->isAllowed(PrivilegePrivileges::getResourceId(PrivilegePrivileges::PRIVILEGE_AFFECTER))
            ? sprintf(
                '<a href="%s" class="modifier-privilege"
                    data-bs-toggle="tooltip" 
                    data-original-title="<span class=\'text-highlight\'><small>Activer le privilège <br/> <strong><em>%s</em></strong> <br/> pour le rôle <br/> <strong><em>%s</em></strong></small></span>">
                    <i class="fas fa-times-circle text-danger"></i>
                </a>',
                $this->view->url('unicaen-privilege/attribuer', ['role' => $role->getId(), 'privilege' => $this->privilege->getId()], [], true),
                $this->privilege,
                $role
            )
            : sprintf(
                '<a data-bs-toggle="tooltip" 
                    data-original-title="<span class=\'text-highlight\'><small>Privilège <br/> <strong><em>%s</em></strong> <br/> Rôle <br/> <strong><em>%s</em></strong></small></span>">
                    <i class="fas fa-times-circle text-danger"></i>
                </a>',
                $this->privilege,
                $role
            );
    }

    /**
     * @param RoleInterface $role
     * @return string
     */
    public function renderLienSupprimer($role)
    {
        return $this->view->isAllowed(PrivilegePrivileges::getResourceId(PrivilegePrivileges::PRIVILEGE_AFFECTER))
            ? sprintf(
                '<a href="%s" class="modifier-privilege"
                    data-bs-toggle="tooltip" 
                    data-original-title="<span class=\'text-highlight\'><small>Désactiver le privilège <br/> <strong><em>%s</em></strong> <br/> pour le rôle <br/> <strong><em>%s</em></strong></small></span>">
                    <i class="fas fa-check-circle text-success"></i>
                </a>',
                $this->view->url('unicaen-privilege/attribuer', ['role' => $role->getId(), 'privilege' => $this->privilege->getId()], [], true),
                $this->privilege,
                $role
            )
            : sprintf(
                '<a data-bs-toggle="tooltip" 
                    data-original-title="<span class=\'text-highlight\'><small>Privilège <br/> <strong><em>%s</em></strong> <br/> Rôle <br/> <strong><em>%s</em></strong></small></span>">
                    <i class="fas fa-check-circle text-success"></i>
                </a>',
                $this->privilege,
                $role
            );
    }

    /**
     * @param RoleInterface $role
     * @return string
     */
    public function renderStatut($role)
    {
        $check = $this->privilege->hasRole($role);
        $html = '<td class="role-privilege-statut text-center %s" style="border-right: 1px solid #ddd;" title="%s">%s</td>';
        return sprintf($html,
            $check ? 'success' : 'danger',
            ($check? 'Ajouter' : 'Retirer') . " le privilège [".$this->privilege->getCategorie()->getLibelle()."|".$this->privilege->getLibelle()."] au rôle [".$role->getLibelle()."]",
            $check ? $this->renderLienSupprimer($role) : $this->renderLienAjouter($role)
        );
    }
}