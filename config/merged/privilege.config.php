<?php

use UnicaenPrivilege\Controller\PrivilegeCategorieController;
use UnicaenPrivilege\Controller\PrivilegeCategorieControllerFactory;
use UnicaenPrivilege\Controller\PrivilegeController;
use UnicaenPrivilege\Controller\PrivilegeControllerFactory;
use UnicaenPrivilege\Form\Privilege\CategorieFiltreForm;
use UnicaenPrivilege\Form\Privilege\CategorieFiltreFormFactory;
use UnicaenPrivilege\Form\Privilege\CategorieForm;
use UnicaenPrivilege\Form\Privilege\CategorieFormFactory;
use UnicaenPrivilege\Form\Privilege\PrivilegeForm;
use UnicaenPrivilege\Form\Privilege\PrivilegeFormFactory;
use UnicaenPrivilege\Provider\Privilege\PrivilegePrivileges;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProvider;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProviderFactory;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieService;
use UnicaenPrivilege\Service\Privilege\PrivilegeCategorieServiceFactory;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceFactory;
use UnicaenPrivilege\View\Privilege\PrivilegeViewHelper;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [

    'bjyauthorize' => [
        'guards' => [
            UnicaenPrivilege\Guard\PrivilegeController::class => [
                [
                    'controller' => PrivilegeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_VOIR,
                    ],
                ],
                [
                    'controller' => PrivilegeController::class,
                    'action' => [
                        'attribuer',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_AFFECTER,
                    ],
                ],
                [
                    'controller' => PrivilegeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_AJOUTER,
                    ],
                ],
                [
                    'controller' => PrivilegeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_MODIFIER,
                    ],
                ],
                [
                    'controller' => PrivilegeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_SUPPRIMER,
                    ],
                ],
                [
                    'controller' => PrivilegeCategorieController::class,
                    'action' => [
                        'index',
                        'gerer',
                        'provider'
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_VOIR,
                    ],
                ],
                [
                    'controller' => PrivilegeCategorieController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_AJOUTER,
                    ],
                ],
                [
                    'controller' => PrivilegeCategorieController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_MODIFIER,
                    ],
                ],
                [
                    'controller' => PrivilegeCategorieController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        PrivilegePrivileges::PRIVILEGE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-privilege' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/privilege',
                    'defaults' => [
                        /** @see PrivilegeController::indexAction() */
                        'controller' => PrivilegeController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'attribuer' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/attribuer/:role/:privilege',
                            'defaults' => [
                                'controller' => PrivilegeController::class,
                                'action' => 'attribuer',
                            ],
                        ],
                    ],
                    'ajouter' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/ajouter/:categorie',
                            'defaults' => [
                                'controller' => PrivilegeController::class,
                                'action' => 'ajouter',
                            ],
                        ],
                    ],
                    'modifier' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/modifier/:privilege',
                            'defaults' => [
                                'controller' => PrivilegeController::class,
                                'action' => 'modifier',
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/supprimer/:privilege',
                            'defaults' => [
                                'controller' => PrivilegeController::class,
                                'action' => 'supprimer',
                            ],
                        ],
                    ],
                    'categorie' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/categorie',
                            'defaults' => [
                                'controller' => PrivilegeCategorieController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route'    => '/ajouter',
                                    'defaults' => [
                                        'controller' => PrivilegeCategorieController::class,
                                        'action'     => 'ajouter',
                                    ],
                                ],
                            ],
                            'gerer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/gerer/:categorie',
                                    'defaults' => [
                                        'controller' => PrivilegeCategorieController::class,
                                        'action'     => 'gerer',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'provider' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/provider/:categorie',
                                    'defaults' => [
                                        'controller' => PrivilegeCategorieController::class,
                                        'action'     => 'provider',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/modifier/:categorie',
                                    'defaults' => [
                                        'controller' => PrivilegeCategorieController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/supprimer/:categorie',
                                    'defaults' => [
                                        'controller' => PrivilegeCategorieController::class,
                                        'action'     => 'supprimer',
                                    ],
                                ],
                                'may_terminate' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            PrivilegeCategorieController::class => PrivilegeCategorieControllerFactory::class,
            PrivilegeController::class  => PrivilegeControllerFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            PrivilegeCategorieService::class => PrivilegeCategorieServiceFactory::class,
            PrivilegeService::class => PrivilegeServiceFactory::class,
            PrivilegeRuleProvider::class => PrivilegeRuleProviderFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
            CategorieFiltreForm::class => CategorieFiltreFormFactory::class,
            CategorieForm::class => CategorieFormFactory::class,
            PrivilegeForm::class => PrivilegeFormFactory::class,
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            'privilege' => PrivilegeViewHelper::class,
        ],
    ],
];