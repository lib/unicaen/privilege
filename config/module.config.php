<?php

use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenPrivilege\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/UnicaenPrivilege/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'UNICAEN__' . __NAMESPACE__,
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            'BjyAuthorize\Service\Authorize'           => \UnicaenPrivilege\Service\AuthorizeServiceFactory::class,
//            'BjyAuthorize\Service\Authorize'           => 'UnicaenAuthentification\Service\AuthorizeServiceFactory', // TODO déplacer le service dans UnicaenAuthentification
        ],
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
