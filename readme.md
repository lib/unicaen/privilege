Module Unicaen Privilege
=======================
------------------------

Description
-----------
Le module **unicaen/privilege** est en charge du stockage des privilèges et de l'affectation de ceux-ci aux rôles de l'application.

Le module fournit un menu dans Administration > Rôle permettant d'accéder aux interfaces d'affectation des privilèges et de gestion des privilèges. 

Description du fonctionnement
============================

Les privilèges sont regroupés en catégorie et en namespace.

Les namespaces
----------

Les namespaces sont simplement des chaînes de caractère servant à regrouper ceux-ci. Ils sont renseignés dans les catégories directement et sont exploités directement dans le système de filtre.

***N.B. :*** Peuvent être détourné pour aider dans l'interface d'affectation. 

Les catégories
----------

Les catégories reposent sur un code unique et possèdent un ordre utiliser pour l'affichage et un éventuel namespace pour le regroupement de ceux-ci.

Les privileges
-------------

Les privilèges reposent sur un code (unique dans sa catégorie), une catégorie et un ordre pour facilité la lecteure de la liste.

L'affectation
--------------

Les affectations passent par l'ajout dans le linker *unicaen_privilege_privilege_role_linker* d'un couple (privilège, rôle).

Configuration
=============

TO BE DONE

Tables pour les données du modules
==================================

**N.B.** Le script permettant de créer les tables est fourni dans le fichier [privilege/SQL/001_tables.sql]

1. **unicaen_privilege_categorie** : table stockant les catégories

| Column | Type | Obligatoire | Unique | Description |
|---|---|---|---|---|
| id | int | true | true | identifiant numerique de la catégorie |
| code | varchar(256)| true | true | identifiant texte de la catégorie |
| libelle | varchar(256) | true | false | libellé associée à la catégorie |
| ordre | int| false | false | ordre de la catégorie |
| namespace | varchar(256)| false | false | namespace contenant la catégorie |

2. **unicaen_privilege_privilege** : table stockant les privilèges

| Column | Type | Obligatoire | Unique | Description |
|---|---|---|---|---|
| id | int | true | true | identifiant numerique du privilège |
| code | varchar(256)| true | true | identifiant texte du privilège |
| libelle | varchar(256) | true | false | libellé associée du privilège |
| categorie_id | int | true | false | catégorie contenant le privilège |
| ordre | int | true | false | ordre du privilège dans sa catégorie  |

3.  **unicaen_privilege_privilege_role_linker** : table stockant les affectations des privilèges aux rôles

| Column | Type | Obligatoire | Unique | Description |
|---|---|---|---|---|
| role_id | int | true | false | identifiant du role lié |
| privilege_id | int | true | false | identifiant du privilège lié |

Privilèges associés au module
=============================

**N.B.** Le script permettant de créer les tables est fourni dans le fichier [privilege/SQL/002_privileges.sql]

```php
const PRIVILEGE_VOIR = 'privilege-privilege_voir';
const PRIVILEGE_AJOUTER = 'privilege-privilege_ajouter';
const PRIVILEGE_MODIFIER = 'privilege-privilege_modifier';
const PRIVILEGE_SUPPRIMER = 'privilege-privilege_supprimer';
const PRIVILEGE_AFFECTER = 'privilege-privilege_affecter';
```

***Attention !!!*** Penser à donner les privilèges aux rôles adéquats.

Dépendance à **UnicaenUtilisateur**
----------------------------------
1. Dependance à `Role` et `RoleInterface` : dans la déclaration des privilèges car lié à un tableau de rôle
- AffectationController.php
- AffectationService.php
- AbstractPrivilege.php
- Privilege.php
- PrivilegeInterface.php
- Mapping des privilèges
   
1. Dependance à `RoleService` et `RoleServiceAwareTrait` : 
- AffectationController.php
- AffectationControllerFactory.php
