
-- DATA
INSERT INTO UNICAEN_PRIVILEGE_CATEGORIE (
    CODE,
    LIBELLE,
    NAMESPACE,
    ORDRE)
    values
        ('utilisateur', 'Gestion des utilisateurs', 'UnicaenUtilisateur\Provider\Privilege', 1),
        ('role', 'Gestion des rôles', 'UnicaenUtilisateur\Provider\Privilege', 2),
        ('privilege', 'Gestion des privilèges', 'UnicaenPrivilege\Provider\Privilege', 3)
    ON CONFLICT (CODE) DO
        UPDATE SET
        LIBELLE=excluded.LIBELLE,
        NAMESPACE=excluded.NAMESPACE,
        ORDRE=excluded.ORDRE;



WITH d(code, lib, ordre) AS (
    SELECT 'utilisateur_afficher', 'Consulter un utilisateur', 1 UNION
    SELECT 'utilisateur_ajouter', 'Ajouter un utilisateur', 2 UNION
    SELECT 'utilisateur_changerstatus', 'Changer le statut d''un utilisateur', 3 UNION
    SELECT 'utilisateur_modifierrole', 'Modifier les rôles attribués à un utilisateur', 4
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
    SELECT cp.id, d.code, d.lib, d.ordre
    FROM d
    JOIN unicaen_privilege_categorie cp ON cp.CODE = 'utilisateur'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
    UPDATE SET
        LIBELLE=excluded.LIBELLE,
        ORDRE=excluded.ORDRE;

WITH d(code, lib, ordre) AS (
    SELECT 'role_afficher', 'Consulter les rôles', 1 UNION
    SELECT 'role_modifier', 'Modifier un rôle', 2 UNION
    SELECT 'role_effacer', 'Supprimer un rôle', 3
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
    SELECT cp.id, d.code, d.lib, d.ordre
    FROM d
    JOIN unicaen_privilege_categorie cp ON cp.CODE = 'role'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
    UPDATE SET
        LIBELLE=excluded.LIBELLE,
        ORDRE=excluded.ORDRE;


WITH d(code, lib, ordre) AS (
    SELECT 'privilege_voir', 'Afficher les privilèges', 1 UNION
    SELECT 'privilege_ajouter' , 'Ajouter un privilège', 2 UNION
    SELECT 'privilege_modifier' , 'Modifier un privilège', 3 UNION
    SELECT 'privilege_supprimer' , 'Supprimer un privilège', 4 UNION
    SELECT 'privilege_affecter' , 'Attribuer un privilège', 5
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
    SELECT cp.id, d.code, d.lib, d.ordre
    FROM d
    JOIN unicaen_privilege_categorie cp ON cp.CODE = 'privilege'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
    UPDATE SET
        LIBELLE=excluded.LIBELLE,
        ORDRE=excluded.ORDRE;

INSERT INTO unicaen_privilege_privilege_role_linker
    (role_id, privilege_id)
    SELECT role.id, privilege.id
    FROM
        unicaen_utilisateur_role role,
        unicaen_privilege_privilege privilege
    JOIN unicaen_privilege_categorie cp ON privilege.categorie_id = cp.id
    WHERE role.role_id = 'Admin_tech'
    AND cp.code IN('utilisateur', 'role', 'privilege')
    ON conflict do nothing;